﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            errorLabel.Visible = false;
            operationLabel.Visible = false;

            if (textBox1.Text.Length != 0)
            {
                operationLabel.Visible = true;
                operationLabel.Text = "+";
            }
            else
            {
                errorLabel.Visible = true;
                errorLabel.Text = "Введите первый аргумент!";
            }
                
        }

        private void button2_Click(object sender, EventArgs e)
        {
            errorLabel.Visible = false;
            operationLabel.Visible = false;

            if (textBox1.Text.Length != 0)
            {
                operationLabel.Visible = true;
                operationLabel.Text = "-";
            }
            else
            {
                errorLabel.Visible = true;
                errorLabel.Text = "Введите первый аргумент!";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            errorLabel.Visible = false;
            operationLabel.Visible = false;

            if (textBox1.Text.Length != 0)
            {
                operationLabel.Visible = true;
                operationLabel.Text = "*";
            }
            else
            {
                errorLabel.Visible = true;
                errorLabel.Text = "Введите первый аргумент!";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            errorLabel.Visible = false;
            operationLabel.Visible = false;

            if (textBox1.Text.Length != 0)
            {
                operationLabel.Visible = true;
                operationLabel.Text = "/";
            }
            else
            {
                errorLabel.Visible = true;
                errorLabel.Text = "Введите первый аргумент!";
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            errorLabel.Visible = false;
            equalLabel.Visible = false;

            if (textBox1.Text.Length != 0 && textBox2.Text.Length != 0)
            {
                equalLabel.Visible = true;
                equalLabel.Text = "=";
            }
            else
            {
                errorLabel.Visible = true;
                errorLabel.Text = "Введите второй аргумент!";
            }

            double x, y, z = 0.0;

            try
            {
                x = double.Parse(textBox1.Text);
                y = double.Parse(textBox2.Text);
                z = 0.0;

                switch(operationLabel.Text)
                {
                    case "+":
                        z = x + y;
                        break;
                    case "-":
                        z = x - y;
                        break;
                    case "*":
                        z = x * y;
                        break;
                    case "/":
                        z = x / y;
                        if (y == 0)
                        {
                            errorLabel.Text = "Ошибка!";
                            errorLabel.Visible = true;
                        }
                        break;               
                }

            }
            catch(Exception ex)
            {
                errorLabel.Text = "Ошибка!";
                errorLabel.Visible = true;
            }

            textBox3.Text = z.ToString();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            operationLabel.Visible = false;
            operationLabel.Text = "null";
            equalLabel.Visible = false;
            equalLabel.Text = "null";
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            errorLabel.Visible = false;
            errorLabel.Text = "null";
        }
    }
}
